#ifndef BMP_WORKER_H
#define BMP_WORKER_H

#include "bmp_header.h"
#include <stdint.h>
#include <stdio.h>

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_EMPTY_IMAGE,
    READ_INVALID_PIXEL,
    READ_FAILED_SKIP_PADDING,
    READ_NULL_POINTER,
    READ_INSUFFICIENT_DATA,
    READ_INVALID_BMP_FORMAT
};

enum read_status from_bmp(FILE *in, struct image *img);

enum read_status read_image_data(FILE *in, struct image *img);

enum read_status read_header_data(FILE *in, struct bmp_header *header);

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_NULL_POINTER
};

enum write_status to_bmp(FILE *out, struct image const *img);

enum write_status write_header_data(FILE *out, struct bmp_header const *header);

enum write_status write_image_data(FILE *out, struct image const *img);
#endif

