#ifndef CONSTANTS_H
#define CONSTANTS_H

#define BM_LITTLE_ENDIAN 0x4D42
#define BI_PLANES 1
#define RGB 24
#define BI_SIZE 40
#define BF_RESERVED 0
#define BI_COMPRESSION 0
#define BI_X_PELS_PER_METER 0
#define BI_Y_PELS_PER_METER 0
#define BI_CRL_USED 0
#define BI_CRL_IMPORTANT 0

#endif
