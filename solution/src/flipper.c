#include "flipper.h"

struct image rotate(struct image const source) {

    struct image rotated = create_image(source.height, source.width);

    if (!rotated.data) {
        return (struct image) {0};
    }

    for (uint64_t i = 0; i < source.width; ++i) {
        for (uint64_t j = 0; j < source.height; ++j) {
            rotated.data[i * source.height + j] = source.data[(source.height - j - 1) * source.width + i];
        }
    }

    return rotated;
}

