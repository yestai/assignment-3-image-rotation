#include "bmp_worker.h"
#include "constants.h"

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};
    enum read_status header_read = read_header_data(in, &header);

    if (header_read != READ_OK) {
        return header_read;
    }

    *img = create_image(header.biWidth, header.biHeight);

    if (!img->data) {
        return READ_EMPTY_IMAGE;
    } else {
        enum read_status status = read_image_data(in, img);

        if (status != READ_OK) {
            free_image(img);
        }

        return status;
    }
}

enum read_status read_image_data(FILE *in, struct image *img) {
    uint8_t padding = count_padding(img->width);

    for (size_t i = 0; i < img->height; i++) {
        size_t read_pixels = fread(img->data + i * img->width, sizeof(struct pixel), img->width, in);

        if (read_pixels != img->width) {
            return READ_INVALID_PIXEL; // Не удалось прочитать полную строку пикселей
        }

        if (fseek(in, padding, SEEK_CUR) != 0) {
            return READ_FAILED_SKIP_PADDING; // Не удалось пропустить padding
        }
    }

    return READ_OK;

}

enum read_status read_header_data(FILE *in, struct bmp_header *header) {
    if (!header || !in) {
        return READ_NULL_POINTER;
    }

    if (fread(header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INSUFFICIENT_DATA;
    }

    if (header->bfType != BM_LITTLE_ENDIAN || header->biBitCount != RGB) {
        return READ_INVALID_BMP_FORMAT;
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = create_header(img);
    enum write_status status = write_header_data(out, &header);

    if (status != WRITE_OK) {
        return status;
    } else {
        return write_image_data(out, img);
    }
}

enum write_status write_header_data(FILE *out, struct bmp_header const *header) {
    if (!header || !out) {
        return WRITE_NULL_POINTER;
    }

    if (fwrite(header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    return WRITE_OK;
}

enum write_status write_image_data(FILE *out, struct image const *img) {
    uint8_t padding = count_padding(img->width);
    uint8_t padding_buffer[4] = {0};

    for (uint64_t i = 0; i < img->height; i++) {
        size_t pixels_written = fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);

        if (pixels_written != img->width) {
            return WRITE_ERROR;
        }

        if (padding > 0) {
            size_t bytes_written = fwrite(padding_buffer, sizeof(uint8_t), padding, out);

            if (bytes_written != padding) {
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}
