#include "bmp_header.h"
#include "constants.h"

struct bmp_header create_header(struct image const *img) {
    uint32_t header_size = sizeof(struct bmp_header);
    uint32_t img_size = img->height * count_padding(img->width) + img->width * sizeof(struct pixel);

    struct bmp_header new_header;

    new_header.bfileSize = header_size + img_size;
    new_header.bOffBits = header_size;
    new_header.biSizeImage = img_size;
    new_header.biWidth = img->width;
    new_header.biHeight = img->height;
    new_header.bfType = BM_LITTLE_ENDIAN;
    new_header.bfReserved = BF_RESERVED;
    new_header.biCompression = BI_COMPRESSION;
    new_header.biPlanes = BI_PLANES;
    new_header.biSize = BI_SIZE;
    new_header.biBitCount = RGB;
    new_header.biXPelsPerMeter = BI_X_PELS_PER_METER;
    new_header.biYPelsPerMeter = BI_Y_PELS_PER_METER;
    new_header.biClrUsed = BI_CRL_USED;
    new_header.biClrImportant = BI_CRL_IMPORTANT;

    return new_header;
}
