#include "bmp_worker.h"
#include "flipper.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "Usage: <source-image> <transformed-image> <angle>\n");
        return EXIT_FAILURE;
    }

    const char *source_file = argv[1];
    const char *transformed_file = argv[2];

    FILE *source = fopen(source_file, "rb");
    if (!source) {
        fprintf(stderr, "Error: Unable to open the source image file.\n");
        return EXIT_FAILURE;
    }

    struct image img = {0};
    enum read_status read_status = from_bmp(source, &img);

    if (read_status != READ_OK) {
        fclose(source);
        fprintf(stderr, "Error: Failed to read the source image.\n");
        return EXIT_FAILURE;
    }

    fclose(source);


    int angle = (atoi(argv[3]) + 360) % 360;

    if (angle != 0 && angle % 90 != 0) {
        fprintf(stderr, "Error: Unsupported angle. Angle should be 0, 90, -90, 180, -180, 270, or -270.\n");
        fclose(source);
        free_image(&img);
        return EXIT_FAILURE;
    }

    int counter = (4 - angle / 90) % 4;

    for (int i = 0; i < counter; ++i) {
        struct image rotated = rotate(img);
        free_image(&img);
        img = rotated;
    }

    FILE *transformed = fopen(transformed_file, "wb");
    if (!transformed) {
        fclose(source);
        fprintf(stderr, "Error: Unable to create the transformed image file.\n");
        return EXIT_FAILURE;
    }

    enum write_status write_status = to_bmp(transformed, &img);

    if (write_status != WRITE_OK) {
        fprintf(stderr, "Error: Failed to write the transformed image.\n");
        fclose(transformed);
        free_image(&img);
        return EXIT_FAILURE;
    }

    free_image(&img);

    return EXIT_SUCCESS;
}

