#include "image.h"
#include <stdlib.h>

struct image create_image(const uint64_t width, const uint64_t height) {
    struct image new_image;

    new_image.width = width;
    new_image.height = height;

    struct pixel *allocated_data = malloc(sizeof(struct pixel) * width * height);

    if (!allocated_data) {
        return (struct image) {0};
    } else {
        for (uint64_t i = 0; i < width * height; ++i) {
            allocated_data[i].r = 0;
            allocated_data[i].g = 0;
            allocated_data[i].b = 0;
        }
        new_image.data = allocated_data;
    }

    return new_image;
}

void free_image(struct image *img) {
    if (img) {
        free(img->data);
    }
}

uint8_t count_padding(uint64_t width) {
    uint64_t bytes_per_row = width * 3;
    uint8_t padding = 0;

    if (bytes_per_row % 4 != 0) {
        padding = 4 - (bytes_per_row % 4);
    }

    return padding;
}
